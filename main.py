import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import pickle

def load_data():
    data_path = "dataset"
    train_data = np.loadtxt(data_path + "/mnist_train.csv", delimiter=",")
    test_data = np.loadtxt(data_path + "/mnist_test.csv", delimiter=",")

    # augmentation
    fac = 0.99 / 255
    train_imgs = np.asfarray(train_data[:, 1:]) * fac + 0.01
    test_imgs = np.asfarray(test_data[:, 1:]) * fac + 0.01
    train_labels = np.asfarray(train_data[:, :1])
    test_labels = np.asfarray(test_data[:, :1])

    # tr_X, tr_Y, ts_X, ts_Y
    return train_imgs, train_labels, test_imgs, test_labels


def show_image(vec):
    image_size = 28
    vec = vec.reshape((image_size, image_size))
    plt.imshow(vec)
    plt.show()


def create_one_hot(tr_Y, ts_Y):
    lr = np.arange(10)
    tr_Y_one_hot = (lr == tr_Y).astype(np.float)
    ts_Y_one_hot = (lr == ts_Y).astype(np.float)
    tr_Y_one_hot[tr_Y_one_hot == 0] = 0.01
    tr_Y_one_hot[tr_Y_one_hot == 1] = 0.99
    ts_Y_one_hot[ts_Y_one_hot == 0] = 0.01
    ts_Y_one_hot[ts_Y_one_hot == 1] = 0.99
    return tr_Y_one_hot, ts_Y_one_hot


def dump_to_pickle(input):
    with open("dataset/mnist.pkl", "bw") as fh:
        pickle.dump(input, fh)

def load_data_from_pickle():
    with open("dataset/mnist.pkl", "br") as fh:
        data = pickle.load(fh)
        return data

def main():
    # execute first time:
    #tr_X, tr_Y, ts_X, ts_Y = load_data()
    #tr_Y_hot, ts_Y_hot = create_one_hot(tr_Y, ts_Y)
    #dump_to_pickle((tr_X, ts_X, tr_Y, ts_Y, tr_Y_hot, ts_Y_hot))

    no_of_different_labels = 10
    data = load_data_from_pickle()
    tr_X = data[0]
    tr_Y = data[1]
    ts_X = data[2]
    ts_Y = data[3]
    tr_Y_hot = data[4]
    ts_Y_hot = data[5]
    print("done")

main()
